
#ifndef FIFO_RINGBUF_H
#define FIFO_RINGBUF_H
#include <stdint.h>

const int FIFO_SIZE = 5;

class Fifo
{
public:
    Fifo();
    int get();
    void put(int item);
    bool is_empty();
    bool is_full();
    void reset();
    uint8_t size();
    int* unittest();
private:
    uint16_t fifo_size;
    int buffer[FIFO_SIZE];
    int* start_ptr;
    int* end_ptr;
    uint8_t buffer_size;
};


Fifo::Fifo()
{
    buffer_size = 0;
    start_ptr = buffer;
    end_ptr = buffer;
}

uint8_t Fifo::size(){
    return buffer_size;
}

int* Fifo::unittest() {
    return buffer;
}

int Fifo::get()
{
    int tmp;
    if(buffer_size>0) {
        if(start_ptr >= (buffer + FIFO_SIZE)) { // if start pointer is buff + 5 which is illegal address, 
            start_ptr = buffer; // then next address is buff
            tmp = *start_ptr;   // and the newest item should be at buff[0]
        }
        else {
            tmp = *start_ptr;   // otherwise we're already at next address where the newest item resides
        }
        start_ptr++;
        buffer_size--;
        return tmp;
    }
    else {
        return 0; // user should check if fifo is empty before calling get()
    }
}

void Fifo::put(int item)
{
    if(buffer_size < FIFO_SIZE) {
        if(end_ptr >= (buffer + FIFO_SIZE)) {
            end_ptr = buffer;
        }
        *end_ptr = item;
        end_ptr++;
        buffer_size++;
    }
    else {
        if(end_ptr >=(buffer+FIFO_SIZE)) { // it's late :|
            int* tmp_ptr = end_ptr;
            tmp_ptr--;
            *tmp_ptr = item;
        }
    }
}

bool Fifo::is_empty()
{
    return buffer_size <= 0;
}

bool Fifo::is_full()
{
    return buffer_size >= FIFO_SIZE;
}

void Fifo::reset()
{
    buffer_size = 0;
    start_ptr = buffer;
    end_ptr = buffer;
}


#endif // FIFO_RINGBUF_H


