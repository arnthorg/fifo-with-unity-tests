
#ifndef FIFO_H
#define FIFO_H
#include <stdint.h>

const int FIFO_SIZE = 5;

class Fifo
{
public:
    Fifo();
    int get();
    void put(int item);
    bool is_empty();
    bool is_full();
    void reset();
    uint8_t size();
private:
    uint16_t fifo_size;
    int buffer[FIFO_SIZE];
    int* buffer_ptr = buffer;
    uint8_t buffer_index;
    //uint8_t buffer_size;
    // add variables pointing to the front and back of the buffer
};


Fifo::Fifo()
{
   buffer_index = 255; 
}

uint8_t Fifo::size()
{
    return buffer_index;
}

int Fifo::get()
{
    if(buffer_index != 255) {
        int tmp = buffer[0];
        for(int i = 1; i<FIFO_SIZE;i++){
            buffer[i-1] = buffer[i];
        }
        buffer_index--;
        return tmp;
    }
    return 0;
}

void Fifo::put(int item)
{
    if(buffer_index != 255 && buffer_index >= FIFO_SIZE) {
        buffer[buffer_index-1] = item;
    }
    else {
        buffer[++buffer_index] = item;
    }
}

bool Fifo::is_empty()
{
    return buffer_index == 255;
}

bool Fifo::is_full()
{
    if(buffer_index == 255) {
        return false;
    }
    return buffer_index >= FIFO_SIZE-1;
}

void Fifo::reset()
{
    buffer_index = 255;
}


#endif // FIFO_H


