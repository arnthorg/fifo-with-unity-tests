#include <Arduino.h>
#include "fifo/fifo_ringbuf.h"
Fifo f;
long t0;
void setup() {
  Serial.begin(9600);
  t0 = millis();
  pinMode(13, OUTPUT);
}

void loop() {
  while(Serial.available()) {
    f.put(Serial.read());
  }
  if((millis()-t0) > 1000) {
    t0 = millis();
    if(!f.is_empty()) {
      Serial.println(f.size());
      Serial.println(f.get());
      }
  }
    if(f.is_full()){
      digitalWrite(13, HIGH);
    }
    else {
      digitalWrite(13, LOW);
    }
}