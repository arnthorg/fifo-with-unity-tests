
#include <util/delay.h>
#include <unity.h>
//#include <fifo/fifo.h>
#include <fifo/fifo_ringbuf.h>

void test_normal_flow(void)
{
    // 1 Setup
    Fifo f;
    f.put(1);
    f.put(2);
    TEST_ASSERT_EQUAL(1, f.get());
    f.put(3);

    // 2-3 Execute and validate
    TEST_ASSERT_EQUAL(2, f.get());
    TEST_ASSERT_EQUAL(3, f.get());

    // 4 Cleanup
}

void test_underflow(void)

{
    // 1 Setup
    Fifo f;
    f.put(1);
    f.put(2);
    f.get();
    f.get();

    // 2-3 Execute and validate
    TEST_ASSERT_TRUE(f.is_empty());

    // 4 Cleanup
}

void test_reset(void) {
    Fifo f;

    TEST_ASSERT_TRUE(f.is_empty());
    f.reset();
    TEST_ASSERT_TRUE(f.is_empty());

    f.put(1);
    f.put(2);
    f.reset();
    TEST_ASSERT_TRUE(f.is_empty());
    f.put(0);
    f.put(1);
    f.put(2);
    f.put(3);

    TEST_ASSERT_EQUAL(0, f.get());
    TEST_ASSERT_EQUAL(1, f.get());
    TEST_ASSERT_EQUAL(2, f.get());
    TEST_ASSERT_EQUAL(3, f.get());
}

void test_overflow(void) {
    Fifo f;
    for(int i=0; i<FIFO_SIZE; i++) {
        f.put(i);
    }
    for(int i=0; i<FIFO_SIZE; i++) {
    TEST_ASSERT_EQUAL(i, f.get());
    }
    for(int i=0; i<FIFO_SIZE; i++) {
        f.put(i);
    }
    for(int i=0; i<FIFO_SIZE; i++) {
    TEST_ASSERT_EQUAL(i, f.get());
    }
    //TEST_ASSERT_EQUAL(5, f.size());
    //TEST_ASSERT_TRUE(f.is_full());
}

void test_overwrite(void) {
    Fifo f;
    for(int i=0; i<FIFO_SIZE*2; i++) {
        f.put(i);
    }
    TEST_ASSERT_TRUE(f.is_full());
    int a;
    for(int i=0; i<FIFO_SIZE; i++) {
        a = f.get();
    }

    TEST_ASSERT_EQUAL(9, a); // síðasta stakið ætti að vera 9 ef elsta stakið var yfirskrifað

}
void test_test(void) { // making sure tests are working
    Fifo f;
    TEST_ASSERT_EQUAL(0, f.size());
    TEST_ASSERT_TRUE(f.is_empty());
}

void test_getorput(void) {
    Fifo f;
    f.unittest();
    for(int i=0; i<FIFO_SIZE; i++) {
        TEST_ASSERT_EQUAL(i, f.get());
    }
    
}

int main()
{
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    _delay_ms(2000); 

    UNITY_BEGIN(); // IMPORTANT LINE!a
    RUN_TEST(test_test);
    RUN_TEST(test_normal_flow);
    RUN_TEST(test_underflow);
    RUN_TEST(test_reset);
    RUN_TEST(test_overflow);
    RUN_TEST(test_overwrite);
    // Add more unit tests here

    UNITY_END(); // stop unit testing
}


